/*
 Stepper Motor Control - speed control

 This program drives a unipolar or bipolar stepper motor.
 The motor is attached to digital pins 8 - 11 of the Arduino.
 A potentiometer is connected to analog input 0.

 The motor will rotate in a clockwise direction. The higher the potentiometer value,
 the faster the motor speed. Because setSpeed() sets the delay between steps,
 you may notice the motor is less responsive to changes in the sensor value at
 low speeds.

 Created 30 Nov. 2009
 Modified 28 Oct 2010
 by Tom Igoe

 */

#include <Stepper.h>

const int stepsPerRevolution = 32 ;  // change this to fit the number of steps per revolution
// for your motor
const int half  = 2048/2;

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

int stepCount = 0;  // number of steps the motor has taken

void setup() {

  Serial.begin(9600);

  turnHalf();
  // nothing to do inside the setup
  Serial.begin(9600);
}


void turnHalf(){
  //https://circuitdigest.com/microcontroller-projects/arduino-stepper-motor-control-tutorial
  myStepper.setSpeed(200);
  myStepper.step(half);
}

void loop() {
  // read the sensor value:
  int sensorValue = analogRead(A0);
  
        Serial.println(sensorValue);  


  if(sensorValue < 100){
          Serial.println(sensorValue);
          
    turnHalf();
  }
  
  
    
  
}
